package com.infosys.poc.poc;

import java.util.PriorityQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Hello world!
 *
 */
public class QueuePOC {
	public static void main(String[] args) {

		PriorityQueue pq = new PriorityQueue();
		pq.add(1);
		pq.add(2);
		pq.add(3);
		pq.add(4);

		
		// to view queue
		System.out.println(pq);

		// to add element at the end of the queue
		pq.add(5);

		// to view queue
		System.out.println(pq);

		// to remove first element in queue
		System.out.println(pq.remove());

		// to view first element in queue
		System.out.println(pq.peek());

		// to view first and remove element in queue
		System.out.println(pq.poll());

		// to view queue
		System.out.println(pq);

System.out.println("===========================================");		
		
		BlockingQueue bq = new PriorityBlockingQueue();
		bq.add(1);
		bq.add(2);
		bq.add(3);
		bq.add(4);

		// to view queue
		System.out.println(bq);

		// to add element at the end of the queue
		bq.add(5);

		// to view queue
		System.out.println(bq);

		// to remove first element in queue
		System.out.println(bq.remove());

		// to view first element in queue
		System.out.println(bq.peek());

		// to view first and remove element in queue
		System.out.println(bq.poll());

		// to view queue
		System.out.println(bq);

		
		
		
		
		
		
		
		
		
		
	}
}
